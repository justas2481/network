package network

import (
	"log"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

// Client holds information about the WebSocket connection
// and pointer that references back to the pool that this client belongs too.
//
// Also it maintains message channel
// which is used to receive messages from pool.
//
// It acts as middleman between the concrete WebSocket and the clients pool.
type client struct {
	// Client unique ID.
	id uuid.UUID

	// Pointer to the Pool that this client belongs to.
	Pool *pool

	// Client connection socket.
	conn *websocket.Conn

	// Message that is to be received from pool and sent
	// to the peers via this channel.
	//
	// This channel is buffered to avoid problems when goroutines are busy with other tasks
	// and can not take info from channel immediately.
	Send chan Message

	// tasksErrC is a comunication pype between client and a task goroutine
	// for the client, invoked by pool and rquested by peer by sending message.
	tasksErrC chan error
}

// readProcess constantly retrieves messages from the socket
// and sends them to the pool for wider broadcast.
//
// This method should be run as separate goroutine
// per client and reading from the socket should happen only there
// to obey the rule that Gorilla WebSocket supports
// read operations concurrently only if one reader is used per connection.
func (c *client) readProcess() {
	// Insure that socket is closed and marked as diactivated
	// when client process ends.
	defer func() {
		// Inform the pool.
		c.Pool.deactivate <- c
		// Then safely close the socket.
		c.conn.Close()
	}()

	// Set time in the future during which packets should arive
	// or connection is considered corrupted.
	if err := c.conn.SetReadDeadline(time.Now().Add(PongTime)); err != nil {
		log.Println("Setting read deadline:", err)
		return
	}

	// Limit the length of a message that can be read from socket.
	c.conn.SetReadLimit(MaxMessageLength)

	// Set pong handler to update time by using current time.
	//
	// Response to ping requests
	// is usually implemented in client websocket libraries.
	c.conn.SetPongHandler(
		func(appData string) error {
			if err := c.conn.SetReadDeadline(time.Now().Add(PongTime)); err != nil {
				return err
			}

			return nil
		})

	// Constantly retrieve messages from the socket.
	for {
		var msg Message
		err := c.conn.ReadJSON(&msg)
		if err != nil {
			log.Println("Error when reading message from socket:", err)
			break
		}

		// Broadcast retrieved message via pool by sending onto channel.
		msg.clientId = c.id
		c.Pool.Broadcast <- msg
	}
}

// writeProcess goroutine of the client constantly listens for messages from pool via send channel
// and sends them to the socket.
//
// This method should be run as separate goroutine
// per client and writing to the socket should happen only there
// to obey the rule that Gorilla WebSocket supports
// write operations concurrently only if one writer is used per connection.
func (c *client) writeProcess() {
	// Timer for a ping requests.
	//
	// Pinging insures that client is connected
	// when it replies with a pong response.
	pingTicker := time.NewTicker(PingInterval)

	// Insure that socket is closed
	// and ping ticker is stopped when client process ends.
	defer func() {
		pingTicker.Stop()
		// We do not need to inform the pool, because it is pool who closes send channel in the first place.
		// Just safely close the socket.
		c.conn.Close()
	}()

	// Constantly check for messages from pull.
	// If there is message on the channel, write it to the socket.
	for {
		select {
		case msg, ok := <-c.Send:
			// Set max write time after which connection is considered corrupt.
			if err := c.conn.SetWriteDeadline(time.Now().Add(MaxWriteTime)); err != nil {
				log.Println("Setting write deadline:", err)
				return
			}

			// Check if pool closed the channel.
			if !ok {
				// Pool says that this client should close the connection,
				// comunication is to slow
				// so send info to the socket and end the goroutine process.
				err := c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				if err != nil {
					log.Println("Writing close message to socket:", err)
				}
				return
			}

			// Write received message to the socket.
			if err := c.conn.WriteJSON(&msg); err != nil {
				log.Println("Unable to encode JSON:", err)
				return // End the goroutine, because client sent invalid message.
			}

			// Issue ping request if time interval had passed.
			// Also update write time deadline.
		case <-pingTicker.C:
			if err := c.conn.SetWriteDeadline(time.Now().Add(MaxWriteTime)); err != nil {
				log.Println("Setting write deadline during ping request:", err)
				return
			}

			// Send ping message.
			if err := c.conn.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
				log.Println("Unable to send ping message:", err)
				return
			}
		}
	}
}
