package network

import (
	"time"

	"github.com/gorilla/websocket"
)

// Server credentials.
const (
	// Default address of the service which it listens to.
	DefaultAddress = "0.0.0.0"

	// Default port of the service.
	DefaultPort = "8000"
)

// Socket write and read limits.
const (
	// Default max buffer size for message reading from socket.
	//
	// Should not be greater than expected MaxMessageLength,
	// because then it will not make any difference in optimizing.
	DefaultReadBuffSize = 1024 // 1 KB per chunk

	// Default max buffer size for message writing to the client.
	DefaultWriteBuffSize = DefaultReadBuffSize

	// MaxMessageLength helps to protect from flood by peer.
	MaxMessageLength = 1024

	// Time that is allowed to write to the socket.
	MaxWriteTime = 10 * time.Second
)

// Ping pong time restrictions.
const (
	// How long to wait after initiating pong request (ping the peer).
	PongTime = 20 * time.Second

	// Send pings after that amount of time had passed.
	//
	// It must be slightly less than pong time,
	// because there should be time left for packets to arive after issuing ping.
	PingInterval = PongTime - (PongTime / 10)
)

// Minimum space for clients in memory to be reserved for a new pool.
const initialClientsInPool = 100

// Default handshake timeout.
// It helps to insure that server will not wait too long
//when connecting with the client.
const DefaultHandshakeTimeout = time.Second * 10

// Buffer size for the send channel
// that transfers messages from pool to particular client.
const SendChanBuff = 128

// Upgrader is used to upgrade http connection to make it websocket.
var upgrader = websocket.Upgrader{
	ReadBufferSize:   DefaultReadBuffSize,
	WriteBufferSize:  DefaultWriteBuffSize,
	HandshakeTimeout: time.Duration(DefaultHandshakeTimeout),
	CheckOrigin:      nil, // Initiate safe default mechanism
}
