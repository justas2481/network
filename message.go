package network

import "github.com/google/uuid"

// message holds the data that's need to be
// sent by pool or received by client via send channel.
type Message struct {
	// Cmd helps to determine
	// what action should be taken after receiving message.
	Cmd uint `json:"cmd"`

	// Message Payload is arbitrary JSON data.
	Payload interface{} `json:"payload,omitempty"`

	// ID of a client which sent this message to the pool.
	//
	// It is not involved in JOSN marshaling and unmarshaling.
	clientId uuid.UUID
}

// ClientId returns client's ID that this message was sent from.
func (m *Message) ClientId() uuid.UUID {
	return m.clientId
}

// SetClientId attaches given client ID to the message
// to indicate client as a sender.
func (m *Message) SetClientId(id uuid.UUID) {
	m.clientId = id
}
