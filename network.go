// WebSocket server used for the networking.
// Implemented to be concurrent and simple.
package network

import (
	"fmt"
	"log"
	"net/http"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

// Pool manages server clients.
//
// Management happens by using channels
// and giving each client a separate read and write goroutine.
type pool struct {
	// Staurage for multiple clients.
	//
	// Hash table is used to quickly insure unique IDs across clients.
	//
	// Keys are 16 byte Universal Unique IDentifiers,
	// as defined in RFC 4122.
	//
	// Values are the  pointers to client instances.
	clients map[uuid.UUID]*client

	// Channel on which clients are to be sent
	// as pointers to be added to the pool.
	//
	// This happens when peer issues a request to the server.
	activate chan *client

	// Channel on which client informs pool to deactivate the client,
	// because peer explicitly requested so.
	deactivate chan *client

	// Broadcast lets pool to receive messages from clients and send them back.
	Broadcast chan Message

	// Application that pool is to be attached to.
	// It should implement Application interface.
	app Application
}

// NewPool creates Pool instance to manage clients and returns pointer to it.
func NewPool(app Application) *pool {
	return &pool{
		clients:    make(map[uuid.UUID]*client, initialClientsInPool),
		activate:   make(chan *client),
		deactivate: make(chan *client),
		Broadcast:  make(chan Message),
		app:        app,
	}
}

// newClient returns pointer to newly instantiated client,
// automatically associated with a pool.
//
// Although client has a pointer to the pool, it is not yet added to the pool.
// That should be done explicitly due to the goroutines and concurrency
// to avoid read / write issues on the clients map.
func (p *pool) newClient(id uuid.UUID, conn *websocket.Conn) *client {
	return &client{
		id:   id,
		Pool: p,
		conn: conn,
		// Buffered channel to insure that connection would not break
		// when client is busy with a socket operations.
		Send:      make(chan Message, SendChanBuff),
		tasksErrC: make(chan error), // One task per client.
	}
}

// addClient adds client to the pool and checks if client's ID is unique.
func (p *pool) addClient(client *client) error {
	// Check if ID is not a duplicate,
	// although probability is rather low when UUID is generated.
	if _, err := p.Client(client.id); err == nil {
		return fmt.Errorf(
			"id %v which was assigned to client is not unique",
			client.id,
		)
	}

	// Add pointer of the given client to pool
	// and assign it's id as map key for retreval.
	p.clients[client.id] = client

	return nil
}

// Client retrieves Client from a Pool by the given id.
func (p *pool) Client(id uuid.UUID) (*client, error) {
	var val *client
	var ok bool
	if val, ok = p.clients[id]; !ok {
		return nil, fmt.Errorf(
			"client with %v id does not exist in the pool",
			id,
		)
	}

	return val, nil
}

// RemoveClient removes the client from a pool.
func (p *pool) removeClient(client *client) error {
	// Check if there is a client to be removed.
	if _, err := p.Client(client.id); err != nil {
		return err
	}

	delete(p.clients, client.id)

	return nil
}

// StartNewConnection hijacks client headers
// to initiate WebSocket connection instead of HTTP. It makes a client by using newly formed
// connection and sends it onto channel to be retrieved later by the pool
// when it detects client waiting on the channel.
//
// It also invokes write and read processes as separate goroutines per client.
func (p *pool) StartNewConnection(id uuid.UUID, w http.ResponseWriter, r *http.Request) error {
	// Create new connection.
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return err
	}

	// Create client based on WebSocket connection.
	client := p.newClient(id, conn)

	// Send client over a channel to pool's goroutine to make client active.
	p.activate <- client

	// Run write and read processes concurrently to help comunicating
	// between pool and the socket.
	go client.readProcess()
	go client.writeProcess()

	return nil
}

// Start invokes a process which listens for
// messages, activation and deactivation of the clients.
//
// Each particular event is sent on a specified channel by the client
// and allows pool to react on them acordingly.
//
// This method can run concurrently as a separate goroutine,
// but read / write operations to the clients map can happen only there,
// solely to avoid mutexes.
func (p *pool) Start() {
	log.Printf("Server is listening on %s:%s...", DefaultAddress, DefaultPort)

	// Start app as a separate goroutine to listen for tasks given by pool.
	go p.app.Run()

	// Listen for events.
	for {
		select {
		// New client is on the channel asking to be added to the pool.
		case client := <-p.activate:
			if err := p.addClient(client); err != nil {
				log.Println("Client activation:", err)
			} else {
				log.Println("New client added. ID:", client.id)
			}

		// Client informs that peer closed the connection.
		case client := <-p.deactivate:
			if err := p.removeClient(client); err != nil {
				log.Println("Deactivation:", err)
			} else {
				log.Println("Client removed. ID:", client.id)
			}

		// Act if there are messages from any client to be broadcasted.
		case msg := <-p.Broadcast:
			// Let application to deal with received messages.
			p.app.GetMessageChan() <- &msg

			// Send it to all active clients.
			for id, client := range p.clients {
				// If writeProcess goroutine is receiving data from client's send channel
				// then send message, otherwise - close channel to inform client
				// that it's time to close the connection
				// and remove client from the clients map.
				select {
				// Send message if possible.
				case client.Send <- msg:
					fmt.Println("Sent to channel for id ", id)

				// If client is not active or fool
				// (which is considered not active as well).
				default:
					close(client.Send)
					if err := p.removeClient(client); err != nil {
						log.Println("Removing client: ", err)
					}
				}
			}

		// Check if there are tasks that happened to fail in the app.
		case err := <-p.app.GetErrorChan():
			if err != nil {
				log.Println("Error when doing task for a client:", err)
			}

		}
	}
}
