// registry allows to register functions as handlers
// for the events that are requested / triggered by the pool mechanism.
//
// It is also used for registering amount of tasks to the given clients
// so that caller can control
// how many of them should be allowed to be executed at once.
package registry

import (
	"errors"

	"github.com/google/uuid"
	"gitlab.com/justas2481/network"
)

// handler is the function that should be triggered
// after particular action requested by the client.
//
// Handler should be adapted to be run concurrently,
// so errors that happen are sent through channel.
type handler func(msg *network.Message, errC chan<- error)

// Commands holds all the registered Commands and their handlers as actions.
type Commands map[uint]handler

// Register adds given command bound with  handler to the commands map
// for later to be invoked by that command.
func (c Commands) Register(cmd uint, action handler) error {
	// Insure that this command is not registered yet.
	if _, exists := c[cmd]; exists {
		return errors.New("command already registered")
	}

	// Register this command.
	c[cmd] = action

	return nil
}

// Retrieve returns handler in commands map by the given command.
func (c Commands) Retrieve(cmd uint) (handler, error) {
	// Insure that command exists in the commands map.
	action, exists := c[cmd]
	if !exists {
		return nil, errors.New("unregistered command")
	}

	return action, nil
}

// Tasks hold an amount of active tasks of particular client refered to by id.
type Tasks map[uuid.UUID]uint

// Add adds 1 to the tasks counter of a given client.
func (t Tasks) Add(clientId uuid.UUID) {
	t[clientId] += 1
}

// Done subtracts 1 from the tasks counter of a given client.
func (t Tasks) Done(clientId uuid.UUID) {
	t[clientId] -= 1
}

// Number returns the number of the tasks that are assigned to the given client.
func (t Tasks) Number(clientId uuid.UUID) uint {
	return t[clientId]
}
