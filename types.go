package network

// Application is responsible for dealing with messages from the pool.
//
// It is also responsible for managing tasks sent by the pool.
// That is the reason why application must have a channel
// which is used to comunicate with pool.
type Application interface {
	// Processes message and acts on it on its own.
	ProcessMessage(msg *Message) error

	// GetMessageChan returns a channel of type pointer to the message sent by pool.
	GetMessageChan() chan<- *Message

	// GetErrorChan returns a channel
	// which is used to send errors to the pool.
	GetErrorChan() <-chan error

	// Run listens for the delegated tasks from pool.
	Run()
}
